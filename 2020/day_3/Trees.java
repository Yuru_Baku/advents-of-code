import java.io.*;
import java.util.*;

public class Trees{


  public static void main(String[] args) {
    Trees trees =  new Trees(3,1);
    trees.readData();

    trees.setSlope(1,1);
    int a = trees.countTrees();
    trees.setSlope(3,1);
    int b = trees.countTrees();
    trees.setSlope(5,1);
    int c = trees.countTrees();
    trees.setSlope(7,1);
    int d = trees.countTrees();
    trees.setSlope(1,2);
    int e = trees.countTrees();

    System.out.println(a*b*c*d*e);
  }

  private ArrayList<String>map;

  private int horMov;
  private int verMov;
  // start positions
  private int horCur;
  private int verCur;

  public Trees(int horMov, int verMov){
    horCur = 0;
    verCur = 0;
    this.horMov = horMov;
    this.verMov = verMov;
    map =  new ArrayList<>();
  }

  public void setSlope(int x, int y){
    horMov = x;
    verMov = y;
  }

  private void reset(){
    horCur = 0;
    verCur = 0;
  }

  public int countTrees(){
    this.reset();
    int count = 0;
    while(verCur < map.size() ){
      if(this.currentField() == '#'){
        count++;
      }
      this.nextPos();
    }
    return count;
  }

  public void nextPos(){
    horCur =  horCur + horMov;
    verCur = verCur + verMov;
    System.out.printf("x: %d, y: %d\n", horCur, verCur);
  }

  public char currentField(){
    // determine the width of the map
    // to offset the horizontal coordiante accordingly
    int mod = map.get(0).length();
    char current = map.get(verCur).charAt(horCur % mod);
    System.out.println(current);
    return current;
  }


  public void printMap(){
    String fullMap =  "";
    for (String row : map){
      fullMap += row +row + row +row + "\n";
    }
    System.out.println(fullMap);
  }

  private void readData() {
    InputStreamReader inputStream = new InputStreamReader(System.in);
    BufferedReader input = new BufferedReader(inputStream);

    for(int i = 0; i < 323; i++){
      try {
        String row = input.readLine();
        map.add(row);
      } catch(IOException ioe) {
        System.out.println("IO IOException raised");
      }
    }
  }
}
