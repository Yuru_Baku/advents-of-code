import java.io.*;
import java.util.*;


public class Sum{
  ArrayList<Integer> data;

  public static void main(String[] args) {
    System.out.println("I have read:");
    Sum sum =  new Sum();
    sum.readData();
    int result = sum.find2020();
    System.out.println(result);
  }

  public Sum() {
    data =  new ArrayList<>();
  }

  private int find2020() {
    int result = 0;
    for ( int a : data){
      for ( int b : data){
        for ( int c : data){
          if(a + b +c == 2020){
            result = a*b*c;
          }
        }
      }
    }
    return result;
  }

  private void printData(){
    for(Integer num : data){
      System.out.println(num);
    }
  }

  private void readData() {
    InputStreamReader inputStream = new InputStreamReader(System.in);
    BufferedReader input = new BufferedReader(inputStream);

    for(int i = 0; i < 200; i++){
      try {
        int number = Integer.parseInt(input.readLine());
        data.add(number);
      } catch(IOException ioe) {
        System.out.println("IO IOException raised");
      }
    }
  }
}
