import java.io.*;
import java.util.*;

public class PasswordCheck{

  public static void main(String[] args) {
    PasswordCheck check = new PasswordCheck();
    check.readData();
    System.out.println(check.countValid());
  }

  public PasswordCheck() {
    passwords =  new ArrayList<>();
  }

  private ArrayList<Password> passwords;

  private int countValid(){
    int count = 0;

    for(Password password : passwords){
      if(password.isValidPos()){
        count++;
      }
    }
    return count;
  }

  private void readData() {
    InputStreamReader inputStream = new InputStreamReader(System.in);
    BufferedReader input = new BufferedReader(inputStream);


    //reset to 1000
    for(int i = 0; i < 1000; i++){
      System.out.println("Pase line " +i);
      try {
        Password password = new Password(input.readLine());
        passwords.add(password);
      } catch(IOException ioe) {
        System.out.println("IO IOException raised");
      }
    }
  }
}

class Password{
  private int min;
  private int max;
  private char acceptedChar;
  private String password;
  private String rawPassword;

  public Password(String rawPassword) {
    this.rawPassword = rawPassword;
    parsePassword();
  }

  public void parsePassword(){
    System.out.println("--------------------------");
    System.out.println(rawPassword);
    findMin();
    findMax();
    findAcceptedChar();
    findPassword();
    System.out.println("--------------------------");
  }

  private void findMin(){
    System.out.println("Find min");
    String minString = rawPassword.substring(0,2);
    int i = 0;
    String min = "";

    boolean isChecking = true;
    do{
      char c = minString.charAt(i);
      if (Character.isDigit(c)){
        min += c;
      }else {
        isChecking = false;
      }
      i++;
      if(i == minString.length()){
        isChecking = false;
      }
    }while(isChecking);

    this.min = Integer.parseInt(min);
    System.out.println("min is: " + this.min);
  }

  private void findMax(){
    System.out.println("Find max");
    String maxString = rawPassword.substring(2,5);
    String max = "";

    for(int i = 0; i < maxString.length(); i++){
      char c = maxString.charAt(i);
      if(Character.isDigit(c)){
        max += c;
      }
    }

    this.max = Integer.parseInt(max);
    System.out.println("max is: " + this.max);
  }

  private void findAcceptedChar(){
    System.out.println("Find accepted char");

    String acceptedCharString = rawPassword.substring(0,8);

    for(int i = 0; i < acceptedCharString.length(); i++) {
      char checkChar = acceptedCharString.charAt(i);
      if(checkChar == ':'){
        this.acceptedChar = acceptedCharString.charAt(i-1);
        break;
      }
    }

    System.out.println("accepted char is: " + this.acceptedChar);
  }

  private void findPassword(){
    System.out.println("Find actuall password");
    String testString = new String(rawPassword);
    for(int i = 0; i < testString.length(); i++){
      if(testString.charAt(i) == ':'){
        password = testString.substring(i+2, testString.length());
      }
    }

    System.out.println("password is: " + this.password);
  }

  private int count() {
    int count = 0;
    for(char letter : password.toCharArray()){
      if(letter == this.acceptedChar){
        count++;
      }
    }
    return count;
  }

  public boolean isValidCount(){
    int count = count();
    if(count >= min && count <= max){
      return true;
    } else{
      return false;
    }
  }

  private boolean positionIsValid(int position){
      char posChar = this.password.charAt(position);
      return posChar == this.acceptedChar;
  }

  public boolean isValidPos(){
    int pos1 = min-1;
    int pos2 = max-1;
    boolean pos1Valid = positionIsValid(pos1);
    boolean pos2Valid = positionIsValid(pos2);
    if(pos1Valid && !pos2Valid){
      return true;
    }else if (!pos1Valid && pos2Valid){
      return true;
    }else{
      return false;
    }
  }
}
